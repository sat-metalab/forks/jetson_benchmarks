#!/bin/bash
sudo apt-get update
sudo apt-get install -y python3-pip
sudo apt-get install -y cython3
sudo apt-get install -y python3-numpy
sudo apt-get install -y python3-pandas
sudo apt-get install -y python3-matplotlib
sudo apt-get install -y python3-cairocffi
sudo apt-get install -y libnvinfer-bin